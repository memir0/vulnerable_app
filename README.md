# Simple_server

The simplest way to host a static website. Serves anything the client asks for within the build folder. 
Using node and express.js

# How to start
1. Copy & paste your site into the build folder
2. Open a terminal inside the root folder
3. Run "npm install"
4. Run "npm start"
5. Site is now available on http://localhost:3000